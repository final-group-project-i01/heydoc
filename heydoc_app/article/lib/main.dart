import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:article/screens/submit_form.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const String title = 'HomeArticle';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Georgia',
        primaryColor: const Color.fromRGBO(0, 173, 181, 1),
        secondaryHeaderColor: const Color.fromRGBO(79, 232, 180, 1),
      ),
      title: 'HeyDoc App',
      home: const MainArticle(title: title),
      // home: submit_form(),
    );
  }
}

class MainArticle extends StatefulWidget {
  final String title;

  // ignore: use_key_in_widget_constructors
  const MainArticle({
    required this.title,
  });

  @override
  _MainArticleState createState() => _MainArticleState();
}

class _MainArticleState extends State<MainArticle>
    with TickerProviderStateMixin {
  ScrollController listScrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (listScrollController.hasClients) {
              final position = listScrollController.position.minScrollExtent;
              listScrollController.animateTo(
                position,
                duration: Duration(seconds: 1),
                curve: Curves.easeOut,
              );
            }
          },
          isExtended: true,
          tooltip: "Scroll to Top",
          child: const Icon(Icons.arrow_upward),
        ),
        body: ListView(
          controller: listScrollController,
          padding: const EdgeInsets.all(16),
          children: [
            Container(
              child: FutureBuilder<List>(
                future: fetchKutipan(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                        reverse: true,
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: snapshot.data?.length,
                        itemBuilder: (context, index) {
                          String articleWord = snapshot.data![index]['article'];
                          if (articleWord.length > 200) {
                            articleWord = articleWord.substring(0, 200) + '...';
                          }
                          String year =
                              snapshot.data![index]['datetime'].substring(0, 4);
                          String month =
                              snapshot.data![index]['datetime'].substring(5, 7);
                          String date = snapshot.data![index]['datetime']
                              .substring(8, 10);
                          return Container(
                            margin: const EdgeInsets.symmetric(vertical: 20),
                            child: Card(
                              color: Color.fromARGB(255, 228, 246, 243),
                              clipBehavior: Clip.antiAlias,
                              child: Column(
                                children: [
                                  ListTile(
                                    leading: Icon(Icons.dangerous_outlined),
                                    title: Text(snapshot.data![index]['title']),
                                    subtitle: Text(
                                      'Create on ' +
                                          date +
                                          '-' +
                                          month +
                                          '-' +
                                          year,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Text(
                                      articleWord,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ),
                                  ButtonBar(
                                    alignment: MainAxisAlignment.start,
                                    children: [
                                      FlatButton(
                                        textColor: const Color(0xFF6200EE),
                                        onPressed: () async {
                                          String url =
                                              snapshot.data![index]['source'];
                                          if (await launch(url)) {
                                            await launch(url,
                                                forceSafariVC: true,
                                                forceWebView: true);
                                          } else {
                                            throw 'Could not launch $url';
                                          }
                                        },
                                        child: const Text('Read Full Article'),
                                      ),
                                    ],
                                  ),
                                  Image.network(
                                      'https://cdn-2.tstatic.net/tribunnews/foto/bank/images/update-covid-19-update-corona-update-virus-corona-ilustrasi-update.jpg'),
                                ],
                              ),
                            ),
                          );
                        });
                  } else if (snapshot.hasError) {
                    return Text("-->>${snapshot.error}<<--");
                  }
                  return Text("error");
                },
              ),
            ),
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  color: const Color.fromRGBO(0, 173, 181, 1),
                  borderRadius: BorderRadius.circular(10)),
              child: FlatButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => submit_form()));
                },
                child: Text(
                  'Add New Article',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
          ],
        ));
  }

  Future<List> fetchKutipan() async {
    String url = 'http://heyheydoc.herokuapp.com/article/datas?format=json';
    final response = await http.get(Uri.parse(url));
    var data = json.decode(response.body) as List;
    return data;
  }
}
