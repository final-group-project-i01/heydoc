import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../Covid_Drugs_recommendation/models/drugs.dart';
import '/homepage/main_drawer.dart';

class DrugApp extends StatefulWidget {
  static const routeName = '/covid-drug';

  @override
  _DrugAppState createState() => _DrugAppState();
}

class _DrugAppState extends State<DrugApp> {
  List<Drugs> items = [];

  @override
  void initState() {
    // This is called when the widget is created.
    super.initState();
    _fetchDrugs();
  }

  _fetchDrugs() async {
    //fetching the data from the api
    items = [];
    List response = json.decode((await http.get(Uri.parse(
            'https://heyheydoc.herokuapp.com/covid_drug_recommendations/getDrugs/')))
        .body); //decoding the json data
    response.forEach((drug) {
      // looping through the json data
      items.add(Drugs.fromJson(drug));
    });
    setState(() {}); // setting the state of the widget
    print(items);
  }

  @override
  Widget build(BuildContext context) {
    //building the widget
    return Scaffold(
      appBar: AppBar(
        // change the color of the text in the appbar to rgb(238, 238, 238)
        title: const Text(
          'Covid Drug Recommendations',
          style: TextStyle(
              color: Color.fromRGBO(238, 238, 238, 1),
              fontFamily: 'Dosis-SemiBold'),
        ),
        // set the background color of the App Bar to #00ADB5
        backgroundColor: Color.fromRGBO(0, 173, 181, 1),
      ),
      drawer: MainDrawer(),
      body: Center(
        // display the list of drugs in a card view with a title and description for each drug and change the color palette of the card to rgb(57, 62, 70)
        child: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            return Card(
              // make the card have rounded corners
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              // make the card have a shadow
              elevation: 5,
              // set the color of the card to rgb(34, 40, 49, 1 )
              color: Color.fromRGBO(34, 40, 49, 1),
              child: ListTile(
                // make the drug name bold and change the text color to rgb(238, 238, 238)
                title: Text(
                  items[index].Drugs_name,
                  style: TextStyle(
                    fontFamily: 'Dosis-SemiBold',
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(238, 238, 238, 1),
                  ),
                ),
                subtitle: Text(items[index].Drugs_description,
                    style: TextStyle(
                        color: Color.fromRGBO(238, 238, 238, 1),
                        fontFamily: 'Dosis-SemiBold')),
                // change the background color of the image to rgb(238, 238, 238) and create a box decoration to make the image rounded and change the color of the border to rgb(238, 238, 238)
                leading: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color.fromRGBO(238, 238, 238, 1),
                  ),
                  child: Image.network('https://heyheydoc.herokuapp.com/' +
                      items[index].Drugs_image),
                ),
              ),
            );
          },
        ),
      ),
      // create a floating button that takes the user to DataIndonesia page and create the button to be a circle with statistics icon
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.insert_chart),
        backgroundColor: Color.fromRGBO(0, 173, 181, 1),
      ),
    );
  }
}
