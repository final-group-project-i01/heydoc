import 'dart:convert';
import 'package:flutter/material.dart';

// create a class that get data from an API https://apicovid19indonesia-v2.vercel.app/api/indonesia and display it
class DataIndonesia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Indonesia'),
        // create a back button to go back to the previous page when the user click on it and make it a back icon
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        child: FutureBuilder(
          future:
              DefaultAssetBundle.of(context).loadString('https://apicovid19indonesia-v2.vercel.app/api/indonesia'),
          builder: (context, snapshot) {
            List data = json.decode(snapshot.data.toString());
            if (data == null) {
              return Container(
                child: Center(
                  child: Text('Loading...'),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: Column(
                      children: <Widget>[
                        Text(
                          data[index]['attributes']['Indonesia'],
                          style: TextStyle(fontSize: 20.0),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Positif : ' + data[index]['attributes']['positif'],
                          style: TextStyle(fontSize: 15.0),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Sembuh : ' + data[index]['attributes']['sembuh'],
                          style: TextStyle(fontSize: 15.0),
                        ),
                         SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Dirawat : ' + data[index]['attributes']['dirawat'],
                          style: TextStyle(fontSize: 15.0),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Meninggal : ' + data[index]['attributes']['meninggal'],
                          style: TextStyle(fontSize: 15.0),
                        ),
                      ],
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}