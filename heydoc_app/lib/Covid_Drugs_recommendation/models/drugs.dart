import 'package:flutter/material.dart';

class Drugs {
  final int id;
  final String Drugs_name;
  final String Drugs_description;
  final String Drugs_image;

  const Drugs({
    @required this.id,
    @required this.Drugs_name,
    @required this.Drugs_description,
    @required this.Drugs_image,
  });
  factory Drugs.fromJson(Map<String, dynamic> json) {
    return Drugs(
      id: json['id'],
      Drugs_name: json['Drugs_name'],
      Drugs_description: json['Drugs_description'],
      Drugs_image: json['Drugs_image'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'Drugs_name': Drugs_name,
      'Drugs_description': Drugs_description,
      'Drugs_image': Drugs_image,
    };
  }
}
