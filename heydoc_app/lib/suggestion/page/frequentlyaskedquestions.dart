// ignore_for_file: prefer_const_constructors, camel_case_types

import 'package:flutter/material.dart';
import '/homepage/main_drawer.dart';

class faq extends StatelessWidget {
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  static const routeName = '/faq';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.cyan[700],
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Text(
            "",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ),
        drawer: MainDrawer(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(
                    top: 28.0, left: 6.0, right: 6.0, bottom: 6.0),
                child: Text('Frequently Asked Questions',
                    style:
                        TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold)),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 12.0, left: 6.0, right: 6.0, bottom: 6.0),
                  child: ExpansionTile(
                    title: const Text("I don't know what medicine to use?"),
                    children: const <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            left: 12.0, right: 12.0, bottom: 6.0),
                        child: Text(
                          "If you don't know or not sure what medicine to buy, you can visit the consultation section. This section will have you talk to professional doctors with your problem. The doctors should be able to recommend the right medicine for you.",
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                      top: 12.0, left: 6.0, right: 6.0, bottom: 6.0),
                  child: ExpansionTile(
                    title: Text("Can I change my password?"),
                    children: const <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            left: 12.0, right: 12.0, bottom: 6.0),
                        child: Text(
                          "For now, the password of your account cannot be changed. The only way is to create a new account",
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                      top: 12.0, left: 6.0, right: 6.0, bottom: 6.0),
                  child: ExpansionTile(
                    title: Text("How do I create an account?"),
                    children: const <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            left: 12.0, right: 12.0, bottom: 6.0),
                        child: Text(
                          "1. On the navigation bar, click on Account\n2. A drop down menu should appear, click on Registration\n3. Insert all the required information and click Submit\n4. The account has been created and can be used to login to the website",
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                      top: 12.0, left: 6.0, right: 6.0, bottom: 6.0),
                  child: ExpansionTile(
                    title: Text("How do I order on HeyDoc?"),
                    children: const <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            left: 12.0, right: 12.0, bottom: 6.0),
                        child: Text(
                          "1. Login into your account\n2. Click the medicine tab on the navigation bar\n3. Click buy medicine\n4. Pick the medicine\n5. Click add to cart on the bottom right of your screen\n6. Click on the shopping cart on right side\n7. Proceed to checkout",
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
