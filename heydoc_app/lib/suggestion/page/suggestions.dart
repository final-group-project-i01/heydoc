// ignore_for_file: camel_case_types, deprecated_member_use

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:http/http.dart' as http;
import 'package:heydoc_app/main.dart';
import '/homepage/main_drawer.dart';

class suggestions extends StatefulWidget {
  static const routeName = '/suggestion';

  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<suggestions> {
  final _formKey = GlobalKey<FormState>();
  double star = 5;
  String name = "";
  String text = "";
  Map<String, dynamic> averageData = {};

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    http
        .get(Uri.parse("https://heyheydoc.herokuapp.com/suggestions/"))
        .then((value) {
      print(value.body.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan[700],
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text(
          "",
          style: TextStyle(
            fontSize: 20,
          ),
        ),
      ),
      drawer: MainDrawer(),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text('Review Your Experience!',
                      style: TextStyle(
                          fontSize: 25.0, fontWeight: FontWeight.bold)),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "Example: Firlandi Ansyari",
                      labelText: "Name",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Name cannot be empty';
                      }
                      setState(() {
                        name = value;
                      });
                    },
                  ),
                ),
                RatingBar.builder(
                  initialRating: 5,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) => const Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    setState(() {
                      star = rating;
                    });
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.multiline,
                    maxLines: 4,
                    decoration: InputDecoration(
                      hintText:
                          "Example: This is the best health app I ever used!",
                      labelText: "Comment",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Comment cannot be empty';
                      }
                      setState(() {
                        text = value;
                      });
                    },
                  ),
                ),
                RaisedButton(
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: const Color(0xFF00ADB5),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.reset();
                      http.post(
                          Uri.parse(
                              "https://heyheydoc.herokuapp.com/suggestions/"),
                          body: <String, dynamic>{
                            "name": name,
                            "text": text,
                            "star": star.toString(),
                          });
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text("Success"),
                              titleTextStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 20),
                              backgroundColor: Colors.greenAccent,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                              content: Text("Review sent!"),
                            );
                          });
                      await Future.delayed(const Duration(seconds: 2), () {});
                      Navigator.of(context).pushReplacementNamed('/');
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
