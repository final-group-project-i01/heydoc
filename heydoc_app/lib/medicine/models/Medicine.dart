import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Medicine {
  final num id;
  final String categories;
  final String title;
  final String description;
  final num price;
  final String image;
  final String slug;

  const Medicine(
      {@required this.id,
      @required this.categories,
      @required this.title,
      @required this.description,
      @required this.price,
      @required this.image,
      @required this.slug});
  factory Medicine.fromJson(Map<String, dynamic> json) {
    return Medicine(
        id: json['id'],
        categories: json['category'],
        title: json['title'],
        description: json['description'],
        price: json['price'],
        image: json['image'],
        slug: json['slug']);
  }

  get imageurl => null;
  Map<String, dynamic> toJson() => {
        'id': id,
        'categories': categories,
        'title': title,
        'description': description,
        'price': price,
        'image': image,
        'slug': slug
      };
}
