import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class OrderItem {
  final num id;
  final num user;
  final List items;

  const OrderItem({
    @required this.id,
    @required this.user,
    @required this.items,
  });
  factory OrderItem.fromJson(Map<String, dynamic> json) {
    return OrderItem(
      id: json['id'],
      user: json['user'],
      items: json['items'],
    );
  }

  get imageurl => null;
  Map<String, dynamic> toJson() => {
        'id': id,
        'user': user,
        'items': items,
      };
}
