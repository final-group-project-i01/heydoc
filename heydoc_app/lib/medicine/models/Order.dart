import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Order {
  final num id;
  final num user;
  final num item;
  final num quantity;

  const Order(
      {@required this.id,
      @required this.user,
      @required this.item,
      @required this.quantity});
  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
        id: json['id'],
        user: json['user'],
        item: json['item'],
        quantity: json['quantity']);
  }

  get imageurl => null;
  Map<String, dynamic> toJson() =>
      {'id': id, 'user': user, 'items': item, 'quantity': quantity};
}
