import 'package:flutter/material.dart';
import '../screens/category_Medicines_screen.dart';
import '../models/Medicine.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

List order = [];

class ProductItem extends StatefulWidget {
  final String id;
  final String title;
  final num duration;
  final String imageurl;
  final String desc;
  final String slug;
  final num quantity;

  ProductItem(
      {@required this.id,
      @required this.title,
      @required this.duration,
      @required this.imageurl,
      @required this.desc,
      @required this.slug,
      @required this.quantity});

  @override
  State<ProductItem> createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text('${widget.title}'),
          content: Text('${widget.desc}'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ],
        ),
      ),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: 120,
          padding: const EdgeInsets.all(0),
          child: Row(children: [
            Expanded(
              flex: 6,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://heyheydoc.herokuapp.com/${widget.imageurl}'),
                        fit: BoxFit.fill)),
              ),
            ),
            Spacer(
              flex: 1,
            ),
            Expanded(
              flex: 14,
              child: Container(
                padding: const EdgeInsets.only(top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("${widget.title}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold)),
                    Row(
                      children: <Widget>[
                        Text(
                          'Quantity : ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        Text(
                          '${widget.quantity}',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 15),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Price : ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        Text(
                          '${widget.duration * widget.quantity}',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 15),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
