import 'package:flutter/material.dart';
import '../screens/category_Medicines_screen.dart';
import '../models/Medicine.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

List order = [];

class MedicineItem extends StatefulWidget {
  final String id;
  final String title;
  final String duration;
  final String imageurl;
  final String desc;
  final String slug;

  MedicineItem({
    @required this.id,
    @required this.title,
    @required this.duration,
    @required this.imageurl,
    @required this.desc,
    @required this.slug,
  });

  @override
  State<MedicineItem> createState() => _MedicineItemState();
}

class _MedicineItemState extends State<MedicineItem> {
  num _counter = 0;

  createMedicine() async {
    final response = await http.post(
        Uri.parse('http://localhost:8000/buy/datas/orderitem/create'),
        body: {});

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      print(response);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to create order. $response');
    }
  }

  void _incrementCount() {
    order.add(widget.id);
    print(order);
    setState(() {
      _counter++;
    });
    setTotal(num.parse(widget.duration));
    print(totalPrice);
    isChanged = true;
  }

  void _decrementCount() {
    order.remove(widget.id);
    print(order);
    if (_counter != 0) {
      setState(() => _counter--);
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text('${widget.title}'),
          content: Text('${widget.desc}'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ],
        ),
      ),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: 120,
          padding: const EdgeInsets.all(0),
          child: Row(children: [
            Expanded(
              flex: 6,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://heyheydoc.herokuapp.com/${widget.imageurl}'),
                        fit: BoxFit.fill)),
              ),
            ),
            Spacer(
              flex: 1,
            ),
            Expanded(
              flex: 14,
              child: Container(
                padding: const EdgeInsets.only(top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text("${widget.title}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold)),
                    Row(
                      children: <Widget>[
                        Text(
                          'Price : ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        Text(
                          'Rp. ${widget.duration}',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 15),
                        )
                      ],
                    ),
                    Align(
                        alignment: Alignment.bottomRight,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 25, bottom: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Theme.of(context).accentColor),
                                child: Row(
                                  children: [
                                    InkWell(
                                        onTap: () {
                                          _decrementCount();
                                        },
                                        child: Icon(
                                          Icons.remove,
                                          color: Colors.white,
                                          size: 16,
                                        )),
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 3),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 3, vertical: 2),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(3),
                                          color: Colors.white),
                                      child: Text(
                                        '$_counter',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 16),
                                      ),
                                    ),
                                    InkWell(
                                        onTap: () {
                                          _incrementCount();
                                          {
                                            final snackBar = SnackBar(
                                              duration: Duration(seconds: 2),
                                              content: const Text(
                                                  'Item has been added to the cart!'),
                                            );

                                            // Find the ScaffoldMessenger in the widget tree
                                            // and use it to show a SnackBar.
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(snackBar);
                                          }
                                          createMedicine();
                                        },
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.white,
                                          size: 16,
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ))
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
