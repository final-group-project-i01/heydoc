import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../widgets/Medicine_item.dart';
import '../models/Medicine.dart';
import '/homepage/main_drawer.dart';

num totalPrice = 0;
bool isChanged = false;
num adder = 0;

void setTotal(num a) {
  adder = a;
}

class CategoryMedicineScreen extends StatefulWidget {
  static const routeName = '/category-Medicine';

  @override
  _CategoryMedicineScreenState createState() => _CategoryMedicineScreenState();
}

class _CategoryMedicineScreenState extends State<CategoryMedicineScreen> {
  TextEditingController editingController = TextEditingController();

  String categoryTitle;
  List<Medicine> items = [];
  List<Medicine> _newitems = [];
  var _loadedInitData = false;

  void _onItemChanged(String value) {
    setState(() {
      _newitems = items.where((val) {
        return val.title.toLowerCase().contains(value.toLowerCase());
      }).toList();
    });
  }

  @override
  void initState() {
    super.initState();
    _retrieveItems();
  }

  _retrieveItems() async {
    items = [];
    List response = json.decode((await http
            .get(Uri.parse('https://heyheydoc.herokuapp.com/buy/datas/item/')))
        .body);
    response.forEach((element) {
      items.add(Medicine.fromJson(element));
    });
    setState(() {});
    final routeArgs =
        ModalRoute.of(context)?.settings.arguments as Map<String, String>;
    categoryTitle = routeArgs['title'];
    final categoryId = routeArgs['id'];
    items = items.where((medicines) {
      return medicines.categories == categoryId;
    }).toList();
    _newitems = items;
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context)?.settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];
      final categoryId = routeArgs['id'];
      items = items.where((medicines) {
        return medicines.categories == categoryId;
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
            title: Text(categoryTitle),
            backgroundColor: Colors.cyan[700],
            actions: [
              // Navigate to the Search Screen
              IconButton(
                  onPressed: () =>
                      Navigator.of(context).pushReplacementNamed('/product'),
                  icon: Icon(Icons.shopping_cart_sharp))
            ]),
        body: Container(
            child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              onChanged: _onItemChanged,
              controller: editingController,
              decoration: InputDecoration(
                  labelText: "Search",
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25.0)))),
            ),
          ),
          Expanded(
              child: ListView.builder(
            padding: const EdgeInsets.all(8),
            itemBuilder: (ctx, index) {
              return MedicineItem(
                  id: (_newitems[index].id).toString(),
                  title: _newitems[index].title,
                  duration: (_newitems[index].price).toString(),
                  imageurl: (_newitems[index].image).toString(),
                  desc: (_newitems[index].description).toString(),
                  slug: (_newitems[index].slug).toString());
            },
            itemCount: _newitems.length,
          )),
        ])));
  }
}
