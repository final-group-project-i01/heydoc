import 'package:flutter/material.dart';
import '../widgets/Product_item.dart';
import '../models/Medicine.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/OrderItem.dart';
import '../models/Order.dart';
import '/homepage/main_drawer.dart';

num totalPrice = 0;
bool isChanged = false;

class ProductScreen extends StatefulWidget {
  static const routeName = '/product';

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  List<Medicine> items = [];
  List<OrderItem> order_item = [];
  List<Medicine> _newitems = [];
  List<Order> order = [];
  List<num> q_list = [];
  @override
  void initState() {
    super.initState();
    _retrieveItems();
  }

  _retrieveItems() async {
    q_list = [];
    items = [];
    order_item = [];
    order = [];
    List response = json.decode((await http
            .get(Uri.parse('https://heyheydoc.herokuapp.com/buy/datas/item/')))
        .body);
    response.forEach((element) {
      items.add(Medicine.fromJson(element));
    });
    List order_response = json.decode((await http
            .get(Uri.parse('https://heyheydoc.herokuapp.com/buy/datas/order/')))
        .body);
    order_response.forEach((element) {
      order_item.add(OrderItem.fromJson(element));
    });
    List orderitem_response = json.decode((await http.get(
            Uri.parse('https://heyheydoc.herokuapp.com/buy/datas/orderitem/')))
        .body);
    orderitem_response.forEach((element) {
      order.add(Order.fromJson(element));
    });

    setState(() {});
    order = order.where((element) {
      return order_item[0].items.contains(element.id);
    }).toList();

    items.forEach((medicines) {
      for (var i = 0; i < order.length; i++) {
        if (medicines.id == order[i].item) {
          _newitems.add(medicines);
          q_list.add(order[i].quantity);
        }
      }
    });
    if (isChanged == false) {
      countTotals();
      isChanged = true;
    }
  }

  countTotals() {
    _newitems.forEach((element) {
      for (var i = 0; i < q_list.length; i++) {
        {
          totalPrice += (element.price * q_list[i]);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(title: Text('Your Cart'), backgroundColor: Colors.cyan[700]),
        body: Container(
            child: Column(children: <Widget>[
          Expanded(
              child: ListView.builder(
            padding: const EdgeInsets.all(8),
            itemBuilder: (ctx, index) {
              return ProductItem(
                  id: (_newitems[index].id).toString(),
                  title: _newitems[index].title,
                  duration: (_newitems[index].price),
                  imageurl: (_newitems[index].image).toString(),
                  desc: (_newitems[index].description).toString(),
                  slug: (_newitems[index].slug).toString(),
                  quantity: q_list[index]);
            },
            itemCount: _newitems.length,
          )),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.all(24),
                decoration:
                    BoxDecoration(color: Theme.of(context).primaryColor),
                child: Row(children: [
                  Expanded(
                      child: Text(
                    "Total Price:  Rp. ${totalPrice}",
                    style: Theme.of(context).textTheme.button.copyWith(
                        color: Theme.of(context).colorScheme.onBackground),
                  )),
                  Expanded(
                      child: Align(
                          alignment: Alignment.centerRight,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                              foregroundColor:
                                  MaterialStateProperty.all<Color>(Colors.blue),
                            ),
                            onPressed: () {},
                            child: Text('Purchase'),
                          )))
                ]),
              ))
        ])));
  }
}
