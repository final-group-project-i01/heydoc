import 'package:flutter/material.dart';

import './categories_screen.dart';
import '../models/Medicine.dart';
import '/homepage/main_drawer.dart';

class TabsScreen extends StatefulWidget {
  TabsScreen();

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, Object>> _pages;
  num _selectedPageIndex = 0;

  @override
  void initState() {
    _pages = [
      {
        'page': CategoriesScreen(),
        'title': 'HeyDoc',
      },
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title']),
        backgroundColor: Colors.cyan[700],
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPageIndex]['page'],
    );
  }
}
