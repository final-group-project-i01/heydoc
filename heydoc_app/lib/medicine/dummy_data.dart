import 'package:flutter/material.dart';

import './models/category.dart';
import './models/Medicine.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'C',
    title: 'Covid-19',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'A',
    title: 'Alergi',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'H',
    title: 'Herbal',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'V',
    title: 'Vitamin',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'F',
    title: 'Flu',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'S',
    title: 'Suplemen Makanan',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'I',
    title: 'Kebutuhan Ibu',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'D',
    title: 'Diabetes',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
  Category(
    id: 'O',
    title: 'Persendian',
    color: Color.fromRGBO(34, 40, 49, 1),
  ),
];
