import 'package:flutter/material.dart';
import 'package:heydoc_app/suggestion/page/frequentlyaskedquestions.dart';
import 'package:heydoc_app/suggestion/page/suggestions.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../vac-info/main.dart';
import '../Covid_Drugs_recommendation/main.dart';
import '../suggestion/main.dart';
import '../medicine/screens/category_Medicines_screen.dart';
import '../medicine/screens/categories_screen.dart';
import '../medicine/screens/product.dart';
import '../medicine/screens/tabs_screen.dart';
import '../article/main.dart';
import 'package:provider/provider.dart';
import '../homepage/main_drawer.dart';
import '../vac-info/api.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'HeyDoc',
      theme: ThemeData(
        backgroundColor: Colors.cyan[700],
        canvasColor: Color.fromRGBO(238, 238, 238, 1),
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'Dosis',
              fontWeight: FontWeight.bold,
            ),
            headline1: TextStyle(
                color: Color.fromRGBO(238, 238, 238, 1),
                fontSize: 26,
                fontFamily: 'Dosis',
                fontWeight: FontWeight.bold)),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/vac-info': (ctx) => VacPage(),
        '/covid-drug': (ctx) => DrugApp(),
        '/suggestion': (ctx) => suggestions(),
        '/category-Medicine': (ctx) => CategoryMedicineScreen(),
        '/medicine': (ctx) => TabsScreen(),
        '/product': (ctx) => ProductScreen(),
        '/article': (ctx) => MainArticle(),
        '/faq': (ctx) => faq(),
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => MyHomePage(),
        );
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  static const routeName = '/';

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  covidData() async {
    var respones =
        await http.get(Uri.parse("https://api.kawalcorona.com/indonesia"));
    var data = jsonDecode(respones.body);

    return data[0];
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.cyan[700],
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Text(
            "HeyDoc",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ),
        drawer: MainDrawer(),
        body: Column(children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: const Text(
              "Welcome to HeyDoc",
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            child: const Text(
              "-Your personal Covid- 19 assistant-",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: const Color(0xFF000000)),
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(1),
                  spreadRadius: 2,
                  blurRadius: 0,
                  offset: Offset(4, 4), // changes position of shadow
                ),
              ],
            ),
            padding: const EdgeInsets.only(left: 10, right: 10),
            margin: const EdgeInsets.only(top: 50, right: 100, left: 5),
            width: 220,
            height: 220,
            child: Center(
              child: Text(
                "In this pandemic era, HeyDoc is a platform to assist you. With our assistance, we strive to make your daily life easier through this pandemic",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.blueGrey[400],
              border: Border.all(width: 1, color: const Color(0xFF000000)),
              borderRadius: BorderRadius.circular(25),
              boxShadow: [
                BoxShadow(
                  color: Colors.blueGrey.withOpacity(1),
                  spreadRadius: 2,
                  blurRadius: 0,
                  offset: Offset(-4, 4), // changes position of shadow
                ),
              ],
            ),
            margin: const EdgeInsets.only(top: 50, left: 130, right: 40),
            height: 220,
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
                  child: Center(
                    child: Text(
                      "Indonesia's Covid - 19 Cases Right Now :",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Container(
                    padding: const EdgeInsets.all(10),
                    child: FutureBuilder(
                      future: covidData(),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState != ConnectionState.done) {
                          return const CircularProgressIndicator();
                        } else {
                          var data = snapshot.data as Map;

                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Column(
                                children: [
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: Center(
                                            child: const Text(
                                              "Positive : ",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          child: Center(
                                            child: Text(
                                              "${data['positif']}",
                                              textAlign: TextAlign.center,
                                              style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.cyanAccent,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: Center(
                                              child: const Text("Healed : ",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                  ))),
                                        ),
                                        Container(
                                          child: Center(
                                            child: Text(
                                              "${data['sembuh']}",
                                              style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.cyanAccent,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: Center(
                                            child: const Text("Hospitalized : ",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                )),
                                          ),
                                        ),
                                        Container(
                                          child: Center(
                                            child: Text(
                                              "${data['dirawat']}",
                                              style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.cyanAccent,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: Center(
                                            child: const Text(
                                              "Death Cases : ",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          child: Center(
                                            child: Text(
                                              "${data['meninggal']}",
                                              style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.cyanAccent,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]),
                                ],
                              ),
                            ],
                          );
                        }
                      },
                    )),
              ],
              //Your widget here,
            ),
          ),
        ]));
  }
}
