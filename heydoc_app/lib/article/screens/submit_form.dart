// ignore_for_file: deprecated_member_use
import '/article/main.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:provider/provider.dart';
import 'dart:convert' as convert;
import '/homepage/main_drawer.dart';

class submit_form extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}

class FormScreenState extends State<submit_form> {
  final _formKey = GlobalKey<FormState>();
  var title = "";
  var article = "";
  var source = "";
  Future<http.Response> sendArticle(var data) {
    return http.post(
      Uri.parse("http://heyheydoc.herokuapp.com/article/addArticle"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: data,
    );
  }

  var currentFocus;
  void _processData() {
    // Process your data and upload to server
    _formKey.currentState?.reset();
  }

  unfocus() {
    currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar:
            AppBar(title: Text('HeyDoc'), backgroundColor: Colors.cyan[700]),
        drawer: MainDrawer(),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(30),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(16),
                  child: Text("Submit Form", style: TextStyle(fontSize: 30)),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Title",
                    icon: Icon(Icons.title),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  onChanged: (String value) {
                    setState(() {
                      title = value;
                    });
                  },
                  onSaved: (String value) {
                    setState(() {
                      title = value;
                    });
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Title cannot be empty';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Article",
                    icon: Icon(Icons.article),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  maxLines: 6,
                  onChanged: (String value) {
                    setState(() {
                      article = value;
                    });
                  },
                  onSaved: (String value) {
                    setState(() {
                      article = value;
                    });
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Article cannot be empty';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Source",
                    icon: Icon(Icons.source),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  onChanged: (String value) {
                    setState(() {
                      source = value;
                    });
                  },
                  onSaved: (String value) {
                    setState(() {
                      source = value;
                    });
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Source cannot be empty';
                    }
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                          child: Text("Submit"),
                          onPressed: () {
                            if (!_formKey.currentState.validate()) return;
                            Navigator.of(context).pushReplacementNamed('/');
                            var data = convert.jsonEncode({
                              'title': title,
                              'article': article,
                              'source': source
                            });
                            FutureBuilder futureBuilder = FutureBuilder<
                                    http.Response>(
                                future: sendArticle(data),
                                builder: (context, snapshot) {
                                  if (snapshot.hasError) {
                                    return AlertDialog(
                                        title: const Text('Error'),
                                        content:
                                            const Text('An error has occured'));
                                  } else if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    var data =
                                        convert.jsonDecode(snapshot.data.body);
                                  }
                                  // ignore: empty_statements
                                  color:
                                  Colors.green;
                                  shape:
                                  RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(color: Colors.green));

                                  return CircularProgressIndicator();
                                });
                          }),
                      SizedBox(width: 30),
                      RaisedButton(
                        child: Text("Clear"),
                        onPressed: () => _processData(),
                        color: Colors.blueGrey[100],
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
