import 'package:flutter/material.dart';
import 'create.dart';
import 'VaccPlace.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'dart:convert';
import '/homepage/main_drawer.dart';

List<VaccPlace> place = [];
int indexplace = 0;

class VacPage extends StatefulWidget {
  static const routeName = '/vac-info';

  @override
  _VacPageState createState() => _VacPageState();
}

class _VacPageState extends State<VacPage> {
  Client client = http.Client();

  void initState() {
    _retrievePlace();
    super.initState();
  }

  _retrievePlace() async {
    var url = Uri.parse(
        "http://heyheydoc.herokuapp.com/vaccination_info/get/place/?format=json");
    place = [];
    List response = json.decode((await http.Client().get(url)).body);
    response.forEach((element) {
      place.add(VaccPlace.fromMap(element));
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan[700],
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text(
          "Vaccination Place",
          style: TextStyle(
            fontSize: 20,
          ),
        ),
      ),
      drawer: MainDrawer(),
      body: ListView.builder(
        itemCount: place.length,
        itemBuilder: (BuildContext context, int index) {
          indexplace = index;
          return ListTile(
            title: Text(place[indexplace].name),
            onTap: () {},
            trailing: IconButton(
              icon: Icon(Icons.border_color_rounded),
              onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => CreatePage(
                        client: client,
                        placeindex: index,
                      ))),
              tooltip: 'increment',
            ),
          );
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
