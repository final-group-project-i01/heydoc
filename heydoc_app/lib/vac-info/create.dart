import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'DaftarVacc.dart';
import 'VaccPlace.dart';
import 'create.dart';
import 'package:http/http.dart' as http;
import 'main.dart';

String dropvalue = '';

class CreatePage extends StatefulWidget {
  final Client client;
  final int placeindex;
  const CreatePage({Key key, @required this.client, @required this.placeindex})
      : super(key: key);

  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  @override
  void initState() {
    super.initState();
    int indexplace = widget.placeindex;
  }

  TextEditingController daftarNameController = TextEditingController();
  TextEditingController daftarNIKController = TextEditingController();
  TextEditingController daftarVaccDateController = TextEditingController();
  TextEditingController daftarVaccPlaceController = TextEditingController();
  var url =
      Uri.parse('https://heyheydoc.herokuapp.com/vaccination_info/createForm/');
  @override
  String dropdownValue = place[0].name;
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff00adb5),
        title: Text("Register for Covid Vaccine"),
      ),
      body: Column(
        children: [
          SizedBox(height: 15),
          Text('Vaccination Place'),
          DropdownButton<String>(
              value: place[widget.placeindex].name,
              icon: const Icon(Icons.arrow_downward),
              elevation: 16,
              style: const TextStyle(color: Colors.black),
              underline: Container(
                height: 2,
                color: Colors.black,
              ),
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue = newValue;
                });
              },
              items: place.map((VaccPlace vaccplace) {
                dropvalue = vaccplace.name.toString();
                return DropdownMenuItem<String>(
                  value: vaccplace.name.toString(),
                  child: Text(vaccplace.name.toString()),
                );
              }).toList()),
          Text('Name'),
          TextField(
            controller: daftarNameController,
          ),
          Text('NIK'),
          TextField(
            controller: daftarNIKController,
          ),
          Text('Date (YYYY-MM-DD)'),
          TextField(
            controller: daftarVaccDateController,
          ),
          ElevatedButton(
              onPressed: () {
                widget.client.post(url, body: {
                  'name': daftarNameController.text,
                  'NIK': daftarNIKController.text,
                  'VaccDate': daftarVaccDateController.text,
                  'VaccPlace': dropdownValue.toString()
                });
                Navigator.pop(context);
              },
              child: Text("Register")),
        ],
      ),
    );
  }
}
