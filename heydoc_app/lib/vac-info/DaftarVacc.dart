import 'dart:convert';
import 'package:flutter/material.dart';

import 'VaccPlace.dart';

class DaftarVacc {
  String name;
  String NIK;
  String VaccDate;
  String VaccPlace;
  DaftarVacc({
    @required this.name,
    @required this.NIK,
    @required this.VaccDate,
    @required this.VaccPlace,
  });

  DaftarVacc copyWith({
    String name,
    String NIK,
    String VaccDate,
    String VaccPlace,
  }) {
    return DaftarVacc(
      name: name ?? this.name,
      NIK: NIK ?? this.NIK,
      VaccDate: VaccDate ?? this.VaccDate,
      VaccPlace: VaccPlace ?? this.VaccPlace,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'NIK': NIK,
      'VaccDate': VaccDate,
      'VaccPlace': VaccPlace,
    };
  }

  factory DaftarVacc.fromMap(Map<String, dynamic> map) {
    return DaftarVacc(
      name: map['name'] ?? '',
      NIK: map['NIK'] ?? '',
      VaccDate: map['VaccDate'] ?? '',
      VaccPlace: map['VaccPlace'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory DaftarVacc.fromJson(String source) =>
      DaftarVacc.fromMap(json.decode(source));

  @override
  String toString() {
    return 'DaftarVacc(name: $name, NIK: $NIK, VaccDate: $VaccDate, VaccPlace: $VaccPlace)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is DaftarVacc &&
        other.name == name &&
        other.NIK == NIK &&
        other.VaccDate == VaccDate &&
        other.VaccPlace == VaccPlace;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        NIK.hashCode ^
        VaccDate.hashCode ^
        VaccPlace.hashCode;
  }
}
