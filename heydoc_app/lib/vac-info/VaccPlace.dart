import 'dart:convert';

import 'package:flutter/material.dart';

class VaccPlace {
  int id;
  String name;
  VaccPlace({
    @required this.id,
    @required this.name,
  });

  VaccPlace copyWith({
    int id,
    String name,
  }) {
    return VaccPlace(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory VaccPlace.fromMap(Map<String, dynamic> map) {
    return VaccPlace(
      id: map['id']?.toInt() ?? 0,
      name: map['name'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory VaccPlace.fromJson(String source) =>
      VaccPlace.fromMap(json.decode(source));

  @override
  String toString() => 'VaccPlace(id: $id, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is VaccPlace && other.id == id && other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
