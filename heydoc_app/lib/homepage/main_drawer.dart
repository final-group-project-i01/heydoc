import 'package:flutter/material.dart';
import '../main.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        const SizedBox(
          height: 120,
          child: DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.cyan,
            ),
            child: Text(
              "OUR SERVICES",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ),
        ListTile(
          leading: const Icon(Icons.home_outlined),
          title: const Text('Dashboard'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.of(context).pushReplacementNamed('/');
          },
        ),
        ListTile(
          leading: const Icon(Icons.article_outlined),
          title: const Text('Covid - 19 Related Articles'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.of(context).pushReplacementNamed('/article');
          },
        ),
        ListTile(
          leading: const Icon(Icons.medication_outlined),
          title: const Text('Covid Drug Recommendation'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.of(context).pushReplacementNamed('/covid-drug');
          },
        ),
        ListTile(
          leading: const Icon(Icons.local_hospital_outlined),
          title: const Text('Vaccination Info'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.of(context).pushReplacementNamed('/vac-info');
          },
        ),
        ListTile(
          leading: const Icon(Icons.medical_services_outlined),
          title: const Text('Buy Medicine'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.of(context).pushReplacementNamed('/medicine');
          },
        ),
        ListTile(
          leading: const Icon(Icons.supervisor_account_outlined),
          title: const Text('Consultation'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.pop(context);
          },
        ),
        ListTile(
          leading: const Icon(Icons.edit_outlined),
          title: const Text('Suggestion'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.of(context).pushReplacementNamed('/suggestion');
          },
        ),
        ListTile(
          leading: const Icon(Icons.question_answer),
          title: const Text('FAQ'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.of(context).pushReplacementNamed('/faq');
          },
        )
      ],
    ));
  }
}
